'use strict';

const soap = require('soap-as-promised'),
    request = require('request');


/**
 * Correios class
 * @example
 * ```
 *  var Correios = require('node-correios'),
 *  correios = new Correios();
 *
 *  correios.calcPreco(args, function (err, result) {
 *    console.log(result);
 *  });
 * ```
 */
class Calculator {
    constructor() {

        // Default URl's
        this.wsdlURL = 'http://ws.correios.com.br/calculador/CalcPrecoPrazo.asmx?wsdl';
        this.codeURL = 'http://viacep.com.br/ws/{CEP}/json';

        // Default args for price calculation
        this.calcArgs = {
            nCdEmpresa: '',
            sDsSenha: '',
            sCdMaoPropria: 'N',
            nVlValorDeclarado: 0,
            sCdAvisoRecebimento: 'N'
        };
    };

    __call(method, args) {
        console.log(["Call actions", args]);
        return new Promise((resolve, reject) => {
            soap
                .createClient(this.wsdlURL)
                .then(client => client[method](args))
                .then(result => resolve(result[method + "Result"].Servicos.cServico))
                .catch(error => reject(error))
        });
    }

    /**
     * Calculate price with shipping
     * @param {object} args
     *
     * @example
     * ```
     * var args = {
     *   nCdServico: '40010,41106,40215',
     *   // demais parâmetros ...
     * };
     *
     * caluclator.calcPreco(args, function (err, result) {
     *   console.log(result);
     * });
     *
     * ```
     * @return {Promise}
     *
     */
    calcDeliveryTime(args) {
        return this.__call("CalcPrazo", Object.assign({}, this.calcArgs, args))
    }


    /**
     * Calculate price and estimate delivery time
     * @param {object} args
     * @example
     * ```
     * var args = {
     *   nCdServico: '40010,41106,40215',
     *   // demais parâmetros ...
     * };
     *
     * correios.calcPrecoPrazo(args, function (err, result) {
     *   console.log(result);
     * });
     * ```
     */
    calcDeliveryAndPrice(args) {
        return this.__call("CalcPrecoPrazo", Object.assign({}, this.calcArgs, args))
    };

    static getInstance() {
        return new Calculator;
    }
}


module.exports = exports = Calculator;