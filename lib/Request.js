const Calculator = require("./Calculator");
const organizer = require("container-organizer");

class Request {
    constructor() {
        this.calculator = new Calculator();
        this.defaultParams = {
            nCdServico: "",
            sCepOrigem: "",
            sCepDestino: "",
            nVlPeso: "",
            nCdFormato: "1",
            nVlComprimento: "",
            nVlAltura: "",
            nVlLargura: "",
            nVlDiametro: "0",
            nVlValorDeclarado: "",
        }
    }


    organizeAndCalculateItems(args) {
        const {items} = args;
        if (items.length === 0) {
            return Promise.reject(items);
        }

        const container = organizer(items);
        const tasks = [];
        container.map(containerBox => tasks.push(this.makeRequest(containerBox, args)));

        return new Promise((resolve, reject) => {
            Promise
                .all(tasks)
                .then(responses => ({prices: this.getTotal(responses), container}))
                .then(result => resolve(result))
                .catch(error => reject(error));
        })

    }

    makeRequest(box, args) {
        const params = this.getParameters(box, args);
        return this.calculator.calcDeliveryAndPrice(params)
    }

    getParameters(item, params) {
        const {services, codeFrom, codeTo, companyId, companyPass} = params;

        return Object.assign({}, this.defaultParams, {
            nCdServico: services,
            sCepOrigem: codeFrom || "",
            sCepDestino: codeTo || "",
            nVlPeso: item.dimensions.weight,
            nVlComprimento: item.dimensions._length,
            nVlAltura: item.dimensions.height,
            nVlLargura: item.dimensions.width,
            nVlDiametro: "0",
            nVlValorDeclarado: Number(item.price) < 19.50 ? "19.50" : item.price
        });

    }

    getTotal(responses) {
        try {
            return responses.reduce((total, i) => Object.assign({}, total, i.reduce((iTotal, res) => {
                iTotal[res.Codigo] = iTotal[res.Codigo] || {
                    total: 0,
                    external: res,
                    success: Number(res.Erro) > -1,
                    message: res.MsgErro
                };
                iTotal[res.Codigo].total += Number(res.Valor.replace(",", ""));
                return iTotal;
            }, {})), {});
        } catch (e) {
            console.error("RESPONSE REDUCER", e);
            throw e;
        }
    }

    static getInstance() {
        return new Request;
    }


}

module.exports = exports = Request;

/*
{ Codigo: 40010,
      Valor: '25,20',
      PrazoEntrega: '2',
      ValorMaoPropria: '0,00',
      ValorAvisoRecebimento: '0,00',
      ValorValorDeclarado: '0,00',
      EntregaDomiciliar: 'S',
      EntregaSabado: 'N',
      Erro: '0',
      MsgErro: '',
      ValorSemAdicionais: '25,20',
      obsFim: '' }
 */