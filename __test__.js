const fs = require("fs");
const path = require("path");
const random = require('random-name');
const faker = require("faker");
const Request = require("./lib/Request");
const outFolder = path.join(__dirname, "test_out");
fs.existsSync(outFolder) || fs.mkdirSync(outFolder, 0o777);

const req = new Request();

const params = {
    services: "04510,04014",
    codeFrom: "36013010",
    codeTo: "70002900",
    companyId: "",
    companyPass: "",
    items: [
        {
            _id: '58cbfcf588843809644ff984',
            amount: '60',
            itemTotal: '13200',
            itemAmount: '60',
            _length: '34',
            height: '1',
            width: '24',
            weight: '0.03',
            price: '220'
        }
    ]
};

random();

const owner = path.join(outFolder, random.first() + '_' + random.last()) + ".json";


req
    .organizeAndCalculateItems(params)
    .then(e => {
        fs.writeFileSync(owner,
            JSON.stringify(Object.assign({}, e, {
                container: e.container.map(c => Object.assign({}, c, {box: `[...] (${c.box.length} items omited) `})),
                date: new Date()
            }), null, 4));
        console.log(`See resuts in: \n${owner}\nOK`);
        process.exit(0);
    })
    .catch(console.error);