const expect = require("expect.js"),
    Request = require("./../lib/Request"),
    Calculator = require("./../lib/Calculator"),
    should = require("should"), fs = require("fs");
const mock = JSON.parse(fs.readFileSync(__dirname + "/mock.json").toString());
let request;
const params = {
    services: "40010",
    codeFrom: "36015220",
    codeTo: "35436000",
    companyId: "",
    companyPass: "",
    items: [
        {
            "height": null,
            "_length": null,
            "width": null,
            "weight": 0.85,
            "_id": "58a2378b888438268d0da4ef",
            "amount": 3
        }
    ]
};
const container = [];
beforeEach(() => {
    request = new Request();
});

describe("Request class", function () {

    it("shoud be a call with new", function () {
        return Request.should.be.calledWithNew;
    });

    it("shoud be a Request  instance", function () {

        let requestInstance = Request.getInstance();
        let newRequest = new Request();

        requestInstance.should.be.an.instanceOf(Request);
        newRequest.should.be.an.instanceOf(Request);
    });

    it("shoud request.calculator be a Calculator Instance", function () {
        return request.calculator.should.be.an.instanceOf(Calculator);
    })

});

describe("Request methods organizeAndCalculateItems", function () {

    it('shoud be organizeAndCalculateItems a Promise', function () {
        return request.organizeAndCalculateItems({items: []}).should.be.Promise();
    });

    it('should be organizeAndCalculateItems reject with no parameters', function () {
        return request.organizeAndCalculateItems({items: []}).should.be.rejected();
    });

    it('should be organizeAndCalculateItems  resolved', function () {
        return request.organizeAndCalculateItems(params).should.be.fulfilled()
    });
});