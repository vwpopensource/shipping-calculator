const expect = require("expect.js"),
    Calculator = require("./../lib/Calculator"),
    should = require("should"), fs = require("fs");
const mock = JSON.parse(fs.readFileSync(__dirname + "/mock.json").toString());
let calculator;

beforeEach(() => {
    calculator = new Calculator();
});

describe("Calculator class", function () {

    it("shoud be a call with new", function () {
        return Calculator.should.be.calledWithNew;
    });

    it("shoud be a Calculator  instance", function () {

        let calculatorInstance = Calculator.getInstance();
        let newCalculator = new Calculator();

        calculatorInstance.should.be.an.instanceOf(Calculator);
        newCalculator.should.be.an.instanceOf(Calculator);
    });


    it('should have the correios WSDL urls', function () {
        calculator.wsdlURL.should.eql('http://ws.correios.com.br/calculador/CalcPrecoPrazo.asmx?wsdl');
    });

    it('should have default calculation arguments "calcArgs"', function () {
        calculator.calcArgs.should.have.property('nCdEmpresa', '');
        calculator.calcArgs.should.have.property('sDsSenha', '');
        calculator.calcArgs.should.have.property('sCdMaoPropria', 'N');
        calculator.calcArgs.should.have.property('nVlValorDeclarado', 0);
        calculator.calcArgs.should.have.property('sCdAvisoRecebimento', 'N');
    });

});

describe('Correios method  calcDeliveryTime', function () {
    it('shoud be calcDeliveryTime a Promise', function () {
        return calculator.calcDeliveryTime({}).should.be.Promise();
    });

    it('should be calcDeliveryTime reject with no parameters', function () {
        return calculator.calcDeliveryTime({}).should.be.rejected();
    });

    it('should be calcDeliveryTime  resolve with mock data', function () {
        return calculator.calcDeliveryTime(mock.CalcPrazo).should.be.fulfilled();
    });

});

describe("Correios method calcDeliveryAndPrice", function () {

    it('shoud be calcDeliveryAndPrice a Promise', function () {
        return calculator.calcDeliveryAndPrice({}).should.be.Promise();
    });


    it('should be calcDeliveryAndPrice reject with no parameters', function () {
        return calculator.calcDeliveryAndPrice({}).should.be.rejected();
    });

    it('should be calcDeliveryAndPrice  resolve with {}', function () {
        return calculator.calcDeliveryAndPrice(mock.CalcPrecoPrazo).should.be.fulfilledWith(mock.CalcPrecoPrazoResult)
    });
});